//
//  GameScene.swift
//  SushiTowerepract2
//
//  Created by Arya S  Asok on 2019-06-18.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    let cat = SKSpriteNode(imageNamed: "character1")
    let sushiBase = SKSpriteNode(imageNamed:"roll")
    var tower:[SKSpriteNode] = []
    
    override func didMove(to view: SKView) {
        // add background
        let background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        // add cat
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        addChild(cat)
        
        // add base sushi pieces
        sushiBase.position = CGPoint(x:self.size.width*0.5, y: 100)
        addChild(sushiBase)
        
        for i in 0...5
        {
            spawnSushi()
        }
    }
    
    func spawnSushi()
    {
        let numPieces = self.tower.count
        
        let specialSushi = SKSpriteNode(imageNamed: "roll")
        
        if(numPieces == 0)
        {
            specialSushi.position =  CGPoint(x: self.size.width*0.5, y: 200)
        }
        else if( numPieces > 0)
        {
            //
             let previousPiece = self.tower[self.tower.count-1]
            specialSushi.position = CGPoint(
                            x:self.size.width*0.5,
                                y:previousPiece.position.y + 100)
            
        }
        
        let rightStick = SKSpriteNode(imageNamed: "chopstick")
        rightStick.position.x = 80;
        rightStick.xScale = -1
        
        let leftStick = SKSpriteNode(imageNamed: "chopstick")
        leftStick.position.y = -80
        
        specialSushi.addChild(leftStick)
        specialSushi.addChild(rightStick)
        
        addChild(specialSushi)
        
        tower.append(specialSushi)
        
        
    }
    
    override func update(_ currentTime: TimeInterval) {
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // This is the shortcut way of saying:
        //      let mousePosition = touches.first?.location
        //      if (mousePosition == nil) { return }
        guard let mousePosition = touches.first?.location(in: self) else {
            return
        }
        
        print(mousePosition)
        var punchTextures: [SKTexture] = []
        for i in 1...3
        {
            
            let fileName = "charecter\(i)"
            print("Adding: \(fileName) to array")
           
                       punchTextures.append(SKTexture(imageNamed: fileName))
                  }
              punchTextures.append(SKTexture(imageNamed:"character1"))
            
              // 2. Tell Spritekit to use that array to create your animation
                let punchingAnimation = SKAction.animate(
                  with: punchTextures,
                   timePerFrame: 0.1)
    
               // 3. Repeat the animation once
              self.cat.run(punchingAnimation)
        
                // remove the sushi from the tower
        
                // 1. GET THE SUSHI FROM THE TOWER
              if (self.tower.count > 0) {
                        let currentSushi = self.tower[0]  // self.tower.first
            
                        if (currentSushi != nil) {
                                // 2. REMOVE THE SUSHI FROM THE SCREEN
                                    currentSushi.removeFromParent()
                    
                                    // 3. REMOVE THE SUSHI FROM THE TOWER
                                    self.tower.remove(at: 0)
                    
                                    print("Number of pieces left in tower: \(self.tower.count)")
                           }
                 }

        let CENTER = self.size.width/2
     
        if (mousePosition.x < CENTER) {
            // left
            // - move cat to x = 25% of width
            cat.position = CGPoint(x:self.size.width*0.25, y:100)
           // When tap, change the picture of the cat so he changes which hand is raised
            let leftHandUpAction = SKAction.scaleX(to: 1, duration: 0)
            self.cat.run(leftHandUpAction)
        }
        else {
            // right
            // - move cat to x = 80% of width
            cat.position = CGPoint(x:self.size.width*0.80, y:100)
            
            let rightHandUpAction = SKAction.scaleX(to: -1, duration: 0)
            self.cat.run(rightHandUpAction)
        }

        
        
        
        
        
    }
    
}
